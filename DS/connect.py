import addblock
from pymongo import MongoClient

db ="block_chain"
myclient = MongoClient()
mydb = myclient[db]


def reply_to_db(v,ts,n,c_id,req,res):
    print("1")
    try:
        mycol = mydb["reply"]
        print(type(v),type(ts),type(n),type(c_id),type(req),type(res))
        mycol.insert_one({"_id":n,"view":v,"ts":ts,"c_id":c_id,"req":req,"res":res})
        print("2")
    except Exception as e:
        print(e)

def get_reply(n):
    mycol = mydb["reply"]
    replies = mycol.find({"$and":[{"_id":{"$lte":n}},{"_id":{"$gte":n-3+1}}]})
    print("Hello")
    print(type(replies))
    addblock.replytoblock(replies,n)

def reply_to_mongodb(key,doc,ref):
    try:
        (db_name,coll_name,operation,_id,_) = key
        db = db_name
        mydb = myclient[db]
        mycol = mydb[coll_name]
        if(operation == 'insert'):
            x = mycol.insert_one(doc)
            print("Insert Success ",x)
        else:
            x = mycol.update_one(ref,{"$set":doc})
            print("Update Success ",x)
    except Exception as e:
        print(e)