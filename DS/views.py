from kafka import KafkaConsumer
from kafka import TopicPartition
import json
import logic
from subprocess import call
from threading import Thread 

f = 1
obj = logic.savereply()
obj1 = logic.savecheckpoint()
obj2 = logic.saveupdate()

def reply_create(data):
	(Reply,view,ts,c_id,cs_id,_,n,res,req) = data
	if Reply == '_REPLY':
		obj.myfunc(view,ts,cs_id,n,c_id,req,f,res)
		print("Done")			
	else:
		print("Wrong reply message")

def checkpoint_create(data):
	(Checkpoint,view,n,hash_dump,cs_id) = data
	if(Checkpoint == '_CHECKPOINT'):
		obj1.myfunc(n,cs_id,f,view,hash_dump)
		print("Done")
	else:
		print("Wrong Checkpoint Message")

def db_update_create(data):
	print("-----------------------DB_update received-----------------------")
	data = eval(data)
	operation = data['operationType']
	db_name = data['ns']['db']
	coll_name = data['ns']['coll']
	_id = data['documentKey']['_id']
	cs_id = data['replicaId']
	print('replica no ',cs_id)
	ref = data['documentKey']
	if operation == 'insert':
		print("Type of db_update is insert")
		doc = data['fullDocument']
		print(type(operation),type(_id),_id,type(doc),type(ref))
		obj2.myfunc(f,db_name,coll_name,operation,_id,cs_id,doc,ref)
		# print(db_name,coll_name,operation,_id,cs_id,doc)
	elif operation == 'update':
		print("Type of db_update is update")
		doc = data['updateDescription']['updatedFields']
		obj2.myfunc(f,db_name,coll_name,operation,_id,cs_id,doc,ref)
	else:
		print("Invalid db operation")

	

def consumer1():
	print("1 started")
	consumer = KafkaConsumer(group_id='DS',bootstrap_servers=['172.27.80.61:9092']
		,auto_offset_reset='smallest',
		 enable_auto_commit=True)
	# consumer = KafkaConsumer(group_id='DS',bootstrap_servers=['localhost:9092']
	# 	,auto_offset_reset='smallest',
	# 	 enable_auto_commit=True)

	consumer.assign([TopicPartition('ds_txn_reply',1)])
	for msg in consumer:
		x = eval((msg.value).decode())
		reply_create(tuple(x))
		print("from 1 :",x[0:6])
		# print("from 0 :",msg.value)


def consumer0():
	print("0 started")
	consumer = KafkaConsumer(group_id='DS',bootstrap_servers=['172.27.80.61:9092']
		,auto_offset_reset='smallest',
		 enable_auto_commit=True)
	# consumer = KafkaConsumer(group_id='DS',bootstrap_servers=['localhost:9092']
	# 	,auto_offset_reset='smallest',
	# 	 enable_auto_commit=True)

	consumer.assign([TopicPartition('ds_txn_reply',0)])
	for msg in consumer:
		x = eval((msg.value).decode())
		reply_create((tuple((x))))
		print("from 0 :",x[0:6])
		# print("from 0 :",msg.value)

def checkpoint_consumer():
	print("Checkpoint consumer started")
	consumer = KafkaConsumer('ds_checkpoint',group_id='CS',bootstrap_servers=['172.27.80.61:9092']
	,auto_offset_reset='smallest',
	 enable_auto_commit=True)
	# consumer = KafkaConsumer('ds_checkpoint',group_id='CS',bootstrap_servers=['localhost:9092']
	# ,auto_offset_reset='smallest',
	#  enable_auto_commit=True)

	for msg in consumer:
		x = eval((msg.value).decode())
		checkpoint_create(tuple(x['checkpoint']))
		print("Message is %s"%(x['checkpoint']))
		# print("Checkpoint message:",msg.value)

def db_state_consumer():
	print("Db consumer started")
	consumer = KafkaConsumer('db_state',group_id='CS',bootstrap_servers=['172.27.80.61:9092']
	,value_deserializer=lambda m: json.loads(m.decode('ascii')),auto_offset_reset='smallest',
	 enable_auto_commit=True)
	# consumer = KafkaConsumer('db_state',group_id='CS',bootstrap_servers=['localhost:9092']
	# ,value_deserializer=lambda m: json.loads(m.decode('ascii')),auto_offset_reset='smallest',
	#  enable_auto_commit=True)

	for msg in consumer:
		# print(msg.value)
		db_update_create(msg.value)


Thread(target=consumer0).start()
Thread(target=consumer1).start()
Thread(target=checkpoint_consumer).start()
Thread(target=db_state_consumer).start()