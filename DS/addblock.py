import time
import json
import hashlib
from pymongo import MongoClient

db ="block_chain"
myclient = MongoClient()
mydb = myclient[db]

def replytoblock(replies,n):
	try:
		block =[]
		mycol = mydb['chain']
		t = int((n/3)-1)
		rows = mycol.find_one({"_id":t})
		if t==0:
			prevBlock = None
		elif rows == None:
			prevBlock = "Yet_to_make_block"
		else:
			prevBlock = hashlib.sha256(json.dumps(rows).encode('utf-8')).hexdigest()
		for reply in replies:
			p = {"txn_id":reply['_id'],"view":reply['view'],"ts":reply['ts'],"client_id":reply['c_id'],"request":reply['req'],"reply":reply['res']}
			block.append(p)
		body = (json.dumps(block))
		b_hash = hashlib.sha256(body.encode('utf-8')).hexdigest()
		q = {"prevBlock":prevBlock,"Timestamp":time.time(),"bodyhash":b_hash}
		header = json.dumps(q)
		mycol.insert_one({"_id":int((n/3)),"header":header,"body":body})
	except Exception as e:
		print(e)
