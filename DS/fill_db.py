from kafka import KafkaProducer
import json
producer = KafkaProducer(bootstrap_servers=['localhost:9092'],value_serializer=lambda m: json.dumps(m).encode('ascii'))
def on_send_success(record_metadata):
    print("Success")
def on_send_error(excp):
    log.error('I am an errback', exc_info=excp)
for i in range(4):
    msg = {"ns": {"db": "changestream", "coll": "collection"}, "documentKey": {"_id": 14}, "replicaId": i, "operationType": "update", "updateDescription": {"removedFields": [], "updatedFields": {"Nandini": "Singh"}}}
    # msg = {"operationType": "insert", "fullDocument": {"Nandini": "Mishra", "_id": 14}, "documentKey": {"_id": 14}, "ns": {"coll": "collection", "db": "changestream"}, "replicaId": i}
    producer.send('db_state',value=msg).add_callback(on_send_success).add_errback(on_send_error)
producer.flush()
producer = KafkaProducer(retries=5)

